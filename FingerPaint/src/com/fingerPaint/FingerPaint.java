package com.fingerPaint;

import java.io.File;
import java.io.FileOutputStream;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Paint.Style;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class FingerPaint extends Activity {
      
	private ImageView imageView;

	   private Button buttonExit;
	   
	   private Button resetButton;
	   
	   private Bitmap baseBitmap;
	   
	   private Canvas canvas;
	   
	   private static Paint paint;
	   
	   private static int currentColor ; 
       
       private String selectedTool = "Pen";
       
       private ImageButton squareButton ,circleButton,triangleButton;
     
       
	   private Button emailButton, redButton,blackButton,grayButton,darkGrayButton,lightBlackButton, brownButton,lightBlueButton,lightGreenButton,lightRedButton,yellowButton,greenButton,lightRed,blueButton,pinkButton,navyButton ,violetButton,purpleButton,crimonButton;
	   
	   // zoom in and out
	   
	   private static final int NONE = 0;  
	    private static final int MOVE = 1;  
	    private static final int ZOOM = 2;  
	      
	    private static final int ROTATION = 1;  
	    
	    private static final int MENU_EMAIL=3;
	    private static final int MENU_MUSIC=4;
	    private static final int MENU_MUSIC1=5;
	    private static final int MENU_MUTE=6;
	    private static final int MENU_CLOSE =7;
	    private static final int MENU_ABOUT =8;
	    private static final int MENU_HELP = 9;
	 
	    
	    
	      
	    private int m = NONE;  
	    private Matrix matrix = new Matrix();  
	    private Matrix savedMatrix = new Matrix();  
	    private PointF start = new PointF();  
	    private PointF mid = new PointF();  
	   
	    private float old;  
	    private int rotate = NONE;  
	   
	    private Intent intentMusic = new Intent("com.fingerPaint");
	   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		
		 
		
		 squareButton = (ImageButton)findViewById(R.id.imageButton2);  
         
		 circleButton = (ImageButton)findViewById(R.id.imageButton3);
		 
		 triangleButton =(ImageButton)findViewById(R.id. imageButton1);
		
		 
		 emailButton =(Button)findViewById(R.id.button21);
		 
		 resetButton = (Button) findViewById(R.id.button20);
		 
		 buttonExit = (Button) findViewById(R.id.button19);
		
		imageView = (ImageView) findViewById(R.id.imageView);
		
		redButton = (Button)findViewById(R.id.button1);
		
		blackButton = (Button)findViewById(R.id.button10);
		
		grayButton = (Button)findViewById(R.id.button6);
		
		darkGrayButton = (Button)findViewById(R.id.button5);
		
		lightBlackButton = (Button)findViewById(R.id.button11);
		
		brownButton = (Button)findViewById(R.id.button13);
		
		lightBlueButton =(Button)findViewById(R.id.button15);
		
		lightGreenButton =(Button)findViewById(R.id.button14);
		
		
		lightRedButton =(Button)findViewById(R.id.button12);
		
		yellowButton =(Button)findViewById(R.id.button2);
		
		
		
		
		greenButton =(Button)findViewById(R.id.button4);
		
		navyButton =(Button)findViewById(R.id.button3);
		
		lightRed = (Button)findViewById(R.id.button7);
		
		blueButton = (Button)  findViewById(R.id.button8);
		 
		pinkButton = (Button)  findViewById(R.id.button9);
		
		violetButton = (Button)  findViewById(R.id.button17);
		
		purpleButton = (Button)  findViewById(R.id.button18);
		
		crimonButton = (Button)  findViewById(R.id.button16);
		
		
		final Intent emailIntent =  new Intent(this,SendEmail.class);
		
		 
		
		emailButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		
				startActivity(emailIntent);
		}
			
			
		});
		
		
		
		
		triangleButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				selectedTool = "Triangle";
				
			}
			
			
		});
		
		
		
		
		circleButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				selectedTool = "Circle";
				
			}
			
			
		});
		
		
		
		
		 squareButton.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					
					selectedTool = "Rectangle";
					
				}
				
				
			});
			
		
		
		
		navyButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor =Color.rgb(225,255,255);
				
			}
			
			
		});
		
		
		
		crimonButton .setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor =Color.rgb(220,20,60);
				
			}
			
			
		});
		
		
		purpleButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor =Color.rgb(147,112,219);
				
			}
			
			
		});
		
		violetButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor =Color.rgb(138,48,226);
				
			}
			
			
		});
		
		
		
		pinkButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor =Color.rgb(255,192,203);
				
			}
			
			
		});
		
		
		
		blueButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor =Color.BLUE;
				
			}
			
			
		});
		
		

		lightRed .setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor =Color.RED;
				
			}
			
			
		});
		
		
		
		
		lightGreenButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor =Color.GREEN;
				
			}
			
			
		});
		
		
		
		
		greenButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor =Color.GREEN;
				
			}
			
			
		});
		
		
		
		
		
		
		
		
		
		yellowButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor =Color.YELLOW;
				
			}
			
			
		});
		
		
		
		lightRedButton .setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor =Color.rgb(220, 20, 70);
				
			}
			
			
		});
		
		
		

		lightGreenButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor = 0xffADFF2F;
				
			}
			
			
		});
		
		
		
		// light blue
		
		lightBlueButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor = 0xffB0E0E6;
				
			}
			
			
		});
		
		
		
		brownButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor =Color.rgb(255,215,0);
				
			}
			
			
		});
		
		
		
		darkGrayButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor = Color.GRAY;
				
			}
			
			
		});
		
		
		lightBlackButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				currentColor = Color.rgb(105, 105, 105);
			
				
			}
			
			
		});
		
		
		
		// set light gray color 
		grayButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				currentColor = Color.LTGRAY;
				
				
			}
			
			
		});
		
		
		
		
		// set red color 
		redButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor = Color.RED; 
				
			}
			
			
		});
		
		

		blackButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				currentColor = Color.BLACK;
				
			}
			
			
		});
		
		
		
		// reset button
		
		resetButton .setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				canvas.drawColor( Color.WHITE );
				Toast.makeText(FingerPaint.this, "The paint is wiped", Toast.LENGTH_SHORT).show();
				     
				imageView.invalidate();  
				     
			}
			
			
			
		});
		
		
		
		// exit button 
		
		buttonExit.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			//	finish();
      
   			   CloseFingerPaint.getInstance().exit();
				
			}
			
		});
		
	
		

		// create bitmap
		
		baseBitmap = Bitmap.createBitmap(600,1100, Bitmap.Config.ARGB_8888);
		
		
		// create canvas
		canvas = new Canvas(baseBitmap);
		
		canvas.drawColor( currentColor );
	     
		
	  imageView.setOnTouchListener(new OnTouchListener(){

		  float startX;
		  
		  float startY;
		  
		@Override
		public boolean onTouch(View view, MotionEvent event) {
			// TODO Auto-generated method stub
		
			
			

			
			ImageView imageView = (ImageView)view;  
            switch (event.getAction()& MotionEvent.ACTION_MASK) {  
            case MotionEvent.ACTION_DOWN:  
 
                break;  
            case MotionEvent.ACTION_UP:  
            	
            	
            	break;
            	
            	
            case MotionEvent.ACTION_POINTER_UP:  
                m = NONE;  
                break;  
                
                
            case MotionEvent.ACTION_POINTER_DOWN:  
                old = (float)Math.sqrt((event.getX(0)-event.getX(1))*(event.getX(0)-event.getX(1))+(event.getY(0)-event.getY(1))*(event.getY(0)-event.getY(1)));  
                if (old > 10f) {  
                    savedMatrix.set(matrix);  
                    mid.set((event.getX(0)+event.getX(1))/2, (event.getY(0)+event.getY(1))/2);  
                    m = ZOOM;  
                }  
                
                break;
                
            case MotionEvent.ACTION_MOVE:  
                if (m == MOVE)  
                {  
                    if(rotate == NONE) {  
                        savedMatrix.set(matrix);  
                        mid.set(event.getX(), event.getY());  
                        rotate = ROTATION;  
                    }  
                    else {  
                        matrix.set(savedMatrix);  
                        double a = Math.atan((mid.y-start.y)/(mid.x-start.x));  
                        double b = Math.atan((event.getY()-mid.y)/(event.getX()-mid.x));  
                        if ((b - a < Math.PI/2 && b - a > Math.PI / 18)||((b + Math.PI) % Math.PI - a < Math.PI/2 && (b + Math.PI) % Math.PI - a > Math.PI / 18)) {  
                            matrix.postScale((float)0.9, (float)0.9);  
                        }  
                        else if ((a - b < Math.PI / 2 && a - b > Math.PI / 18)||((a + Math.PI) % Math.PI - b < Math.PI/2 && (a + Math.PI) % Math.PI - b > Math.PI / 18)) {  
                            matrix.postScale((float)1.1, (float)1.1);  
                        }  
                        start.set(event.getX(), event.getY());  
                        rotate = NONE;  
                    }  
                }  
                else if(m == ZOOM)  
                {  
                    float newDistance;  
                    newDistance = (float)Math.sqrt((event.getX(0)-event.getX(1))*(event.getX(0)-event.getX(1))+(event.getY(0)-event.getY(1))*(event.getY(0)-event.getY(1)));  
                    if(newDistance > 10f) {  
                        matrix.set(savedMatrix);  
                        matrix.postScale(newDistance/old, newDistance/old, mid.x, mid.y);  
                        old = newDistance;  
                        savedMatrix.set(matrix);  
                    }  
                }  
                break;  
            }  
            imageView.setImageMatrix(matrix);  
			
			
			
			
			
			if(selectedTool.equals("Pen")){
			
	                      
				
			
			
			switch(event.getAction()){
			
			
			// first touch
			case MotionEvent .ACTION_DOWN:
				
				
				
				startX = (float)event.getX();
					
		      	startY = (float)event.getY()-50;
				
				
				break;
			    
		         // move up
			case MotionEvent.ACTION_MOVE:	 
				
				 float newX = (float)event.getX();
				
				 float newY = (float)event.getY()-50;
				
					paint = new Paint();
					paint.setStrokeWidth(20);
					paint.setColor(currentColor);

				
				canvas.drawLine(startX, startY, newX, newY, paint);
				
				
				
				
                 startX = (float)event.getX();
				
				startY = (float)event.getY()-50;
				
				imageView.setImageBitmap(baseBitmap);
				
				break;
				
				// finger leave screen
			case MotionEvent.ACTION_UP:
				
				break;
			 
			}
			
			}  else if(selectedTool.equals("Circle")){
				
				 startX =   event.getX();
					
			     startY =  event.getY()-50;
				
				
				imageView.setImageBitmap(drawCircle(startX,startY));
				
				
		}  else if(selectedTool.equals("Rectangle")){
			

			     startX =   event.getX();
				
		         startY =  event.getY()-50;
		         
		         
		         float x2 = startX+50;
		         
		         float y2 = startY+50;
		         
			
			
		         imageView.setImageBitmap(drawRectangle(startX,y2,x2,startY));
			
		} else if (selectedTool.equals("Triangle")){
			
	               
			    startX =   event.getX();
				
		       startY =  event.getY()-50;

		       imageView.setImageBitmap(drawTriangle(startX,startY));
			
		}
			
			
			
			
			
			
			return true;
		}
		  
		  
		  
		  
	  });
		
		
	
	}
	
	// add menu
	
	public boolean onCreateOptionsMenu(Menu menu){
		
		getMenuInflater().inflate(R.menu.main, menu);
		
		
		menu.add(0,MENU_EMAIL,1,R.string.email);
		menu.add(0,MENU_CLOSE,2,R.string.close);
	    
		SubMenu subMenu =  menu.addSubMenu(0,MENU_MUSIC,3,R.string.music);
		     subMenu.add(0,MENU_MUSIC1,4,R.string.music1);
		     subMenu.add(0,MENU_MUTE,5,R.string.mute);
		menu.add(0,MENU_HELP,6,R.string.help);
		menu.add(0,MENU_ABOUT,7,R.string.about);
		
		
		
		return true;
		
	}
	
	
	public boolean onOptionsItemSelected(MenuItem item){
		
		
		switch(item.getItemId()){
		
		
		case MENU_CLOSE:
			
			stopService(intentMusic);
			CloseFingerPaint.getInstance().addActivity(this);
			CloseFingerPaint.getInstance().exit();
			
		
			return true;
			
			
			
		case MENU_EMAIL:
			
			Intent intent = new Intent(this,SendEmail.class);
			startActivity(intent);
			
			
			return true;
			
		case MENU_ABOUT:
			
			Intent   aboutIntent = new Intent(this,About.class);
			
			startActivity(aboutIntent);
			
			return true;
			
			
		case MENU_HELP:
			
			Intent helpIntent = new Intent(this,Help.class);
			
			startActivity(helpIntent);
			
			return true;
			
		case MENU_MUSIC1:
			
			 startService(intentMusic);
			 return true;
			 
			 
		case MENU_MUTE:
			
			stopService(intentMusic);
			
			return true;
	     		 
		
		}
		
		
	
		
		
		return false;
	}
	
	
	
	
	// save files as photo
	
	public void save(View view){
		
		try {
			File file = new File( Environment.getExternalStorageDirectory(),System.currentTimeMillis()+".jpg");
			
			FileOutputStream stream = new FileOutputStream(file);
			
			baseBitmap.compress(CompressFormat.JPEG, 100, stream);
			
			stream.close();
			
			Toast.makeText(this,"This file is saved",1).show();
			
			
			// mock SD card
			Intent intent = new Intent();
			
			intent.setAction(Intent.ACTION_MEDIA_MOUNTED);
			
			intent.setData(Uri.fromFile(Environment.getExternalStorageDirectory()));
			
			sendBroadcast(intent);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Toast.makeText(this, "failed to save", 1).show();
			e.printStackTrace();
		}
		
		
		
	}
	
	

	
	
	// draw triangle
	
	public void createTriangle(View view){
		
	     paint = new Paint();
		
		
		
		paint.setAntiAlias(true);
			
	    paint.setStyle(Paint.Style.FILL);
	   

		
		baseBitmap= Bitmap.createBitmap(600,1100,Config.ARGB_8888);
		
		
		canvas = new Canvas(baseBitmap);
		 
		 
	}
	
	
	
public Bitmap drawTriangle( float x, float y){
		
		Path path = new Path();
		
		path.moveTo(x, y);
		
		path.lineTo(x, y+60);
		
		path.lineTo(x+51.96f, y+30);
		
		path.close();
		
		canvas.drawPath(path, paint);
		
		paint.setColor(currentColor);
		
		return baseBitmap;
		
		
	}



public void createRectangle (View view){
	
	
	
	
	paint.setAntiAlias(true);
	
	paint.setStrokeWidth(20);
	
	
	paint.setStyle(Style.STROKE);
	
	
	baseBitmap= Bitmap.createBitmap(600,1100,Config.ARGB_8888);
	
	
	canvas = new Canvas(baseBitmap);
	
}


public Bitmap drawRectangle(float x, float y,float x1,float y2){
	
	
	 paint = new Paint();
	 
	 paint.setColor(currentColor);
	 paint.setStyle(Paint.Style.FILL);
     canvas.drawRect(x, y2, x1, y, paint);
	
     
     return  baseBitmap;
}



		public void createCircle(View view){
			
				    
					
					paint.setAntiAlias(true);
					
					paint.setStrokeWidth(5);
					
					
					baseBitmap= Bitmap.createBitmap(600,1100,Config.ARGB_8888);
					
					
					canvas = new Canvas(baseBitmap);
							
			
		}
		
		
		
		public Bitmap drawCircle(float x, float y){
			
			
			paint = new Paint();  
	       
			paint.setColor(currentColor);
	        
	        canvas.drawCircle(x, y, 50, paint);
	        paint.setAntiAlias(true);
	       
	  
			
			return baseBitmap;
		}
		

	

}