package com.fingerPaint;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

/**
 * 
 * 
 * @author Hao
 * This class is for setting background music
 */

public class BackgroundMusic extends Service {

	private MediaPlayer mediaPlayer;

	@Override
	public IBinder onBind(Intent intent) {
	// TODO Auto-generated method stub
	return null;
	}

//	@Override
//	public void onStart(Intent intent,int startId){
//	super.onStart(intent, startId);
//
//		if(mediaPlayer==null){
//	
//		// get background.mp3 from the folder of raw
//		mediaPlayer = MediaPlayer.create(this, R.raw.paparazzi);
//	
//		mediaPlayer.setLooping(true);
//		mediaPlayer.start();
//	
//		}
//	}
	public int onStartCommand(Intent intent, int flags, int startId){
		super.onStart(intent, startId);
		
		
		if(mediaPlayer==null){


			mediaPlayer = MediaPlayer.create(this, R.raw.paparazzi);

			mediaPlayer.setLooping(true);
			mediaPlayer.start();
			
			
		}
		
		 return Service.START_NOT_STICKY; 
		}
	


	@Override
	public void onDestroy() {
	// TODO Auto-generated method stub
	super.onDestroy();
	mediaPlayer.stop();
	}
}
