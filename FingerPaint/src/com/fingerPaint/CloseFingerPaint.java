package com.fingerPaint;

import java.util.LinkedList;
import java.util.List;
import android.app.Activity;
import android.app.Application;

public class CloseFingerPaint extends Application {

	private List<Activity> activityList = new LinkedList<Activity>();
	
	private static CloseFingerPaint instance;
	
	
	private CloseFingerPaint(){
		
	}
	
	
	public static CloseFingerPaint getInstance(){
		
		if(null== instance){
			
			instance = new CloseFingerPaint();
		}
		
		
		
		return instance;
		
		
	}
	
	
	public void addActivity (Activity activity){
		
		
		activityList.add(activity);
	}
	
	public void exit(){
		
		for(Activity activity:activityList){
		
			activity.finish();
		}
		
		System.exit(0);
	 
	}
	
	
}
